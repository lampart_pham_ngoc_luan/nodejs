var me = {};
me.avatar = "https://lh6.googleusercontent.com/-lr2nyjhhjXw/AAAAAAAAAAI/AAAAAAAARmE/MdtfUmC0M4s/photo.jpg?sz=48";

var you = {};
you.avatar = "https://a11.t26.net/taringa/avatares/9/1/2/F/7/8/Demon_King1/48x48_5C5.jpg";

function formatAMPM(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var ampm = hours >= 12 ? 'PM' : 'AM';
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    var strTime = hours + ':' + minutes + ' ' + ampm;
    return strTime;
}

//-- No use time. It is a javaScript effect.
function insertChat(who, text, template, time) {
    var control = "";
    var date = formatAMPM(new Date());

    if (who == "me") {

        control = '<li style="width:100%">' +
            '<div class="msj macro">' +
            '<div class="avatar"><img class="img-circle" style="width:100%;" src="' + me.avatar + '" /></div>' +
            '<div class="text text-l">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '</div>' +
            '</li>';
    } else {
        control = '<li style="width:100%;">' +
            '<div class="msj-rta macro">' +
            '<div class="text text-r">' +
            '<p>' + text + '</p>' +
            '<p><small>' + date + '</small></p>' +
            '</div>' +
            '<div class="avatar" style="padding:0px 0px 0px 10px !important"><img class="img-circle" style="width:100%;" src="' + you.avatar + '" /></div>' +
            '</li>';
    }
    setTimeout(
        function () {

            $('.' + template).find("ul").append(control);

        }, time);

}

function resetChat() {
    $("ul").empty();
}


//-- Clear Chat
resetChat();


//connect tới server
var socket = io.connect('http://127.0.0.1:3000');
var my_socket_id = '';
var users = {};

if (socket) {
    console.log('connected');
}
else {
    console.log('connecte failed');
}
socket.on('connect', function (data) {
    console.log('connect ok ')
})
register();
function register() {
    socket.on('register', function (data) {
        my_socket_id = data;
        console.log('my socket id : ' + my_socket_id);
    })

}
socket.on('users', function (data) {
    console.log(data)
    users = data;
    if(typeof users[my_socket_id] =='undefined'){
        $('#reg_name').modal('show');
        return;
    }
    console.log(users);

    if (users) {
        var html = '';
        $.each(users, function (k, v) {
            var active='';
            var me=''
            if(k==my_socket_id){
                active='active';
                me=' ( me )'
            }
            html += ' <a href="#" class="list-group-item '+active+'" data-id="' + k + '">' + v +me+ '</a>';
        })
        $('.list-group').html(html);
    }
});

socket.on('message', function (data) {
    if(my_socket_id==''){
        $('#reg_name').modal('show');
        return;
    }
    console.log(data)
    var chat_container = $('#chat-container')
    var template = ''

    if (data[0] == 'chatgroup') {
        template = 'chat-group';
    }
    var com_class = data[0] + '-' + data[2];
    if (chat_container.find('.' + com_class).length > 0) {
        template = com_class;
    }
    var com_class = data[2] + '-' + data[0];
    if (chat_container.find('.' + com_class).length > 0) {
        template = com_class;
    }

    if (template == '') {

        if (data[2] == my_socket_id) {
            add_chat_window(data[0])
            template = my_socket_id + '-' + data[0]
        }
        else {

            add_chat_window(data[2])
            template = my_socket_id + '-' + data[2];
            $('.' + template).addClass('new-message')
        }
    }

    if (data[2] == my_socket_id) {
        insertChat("you", '<b>me : </b>' + '<br/>' + data[1], template, 0);
    }
    else {
        $('.' + template).addClass('new-message')
        insertChat("me", '<b>' + users[data[2]] + ' : </b><br/>' + data[1], template, 0);
    }


});

$(function () {

    $('#reg_name').modal('show');
    $('#reg_name').on('shown.bs.modal', function () {
        $('#reg-your-name').focus()
    })

    $('#reg-button').on('click', function () {
        if ($('#reg-your-name').val() == '') {
            alert('please input your name !');
            return;
        }
        socket.emit('register', $('#reg-your-name').val());
        $('#reg_name').modal('hide');
    })


    $('#list-group ').delegate('.list-group-item', 'click', function () {

        var socket_id_to = $(this).data('id');
        if (socket_id_to == my_socket_id) {
            alert('You can not chat with myseft')
            return;
        }

        add_chat_window(socket_id_to)

    })

    $('#chat-container').delegate('.private-button', 'click', function () {
        send_message(this)
    })

    $('#chat-container').delegate('.private-message', 'keyup', function (e) {
        if (e.which == 13) {
            send_message(this)
        }
    })

    $('#chat-container').delegate('.private-message','focus',function(){

        var template=$(this).parents('.template')
        if(template.hasClass('new-message')){
            template.removeClass('new-message')
        }
    })

})
function send_message(e) {
    if(my_socket_id==''){
        $('#reg_name').modal('show');
        return;
    }
    var template = $(e).parents('.template');
    var id = template.find('.private-id').val();
    var message = template.find('.private-message').val();
    if (message == '') {
        alert('please input message !')
        return;
    }
    console.log('send message: ' + message + ' to :' + id)
    socket.emit('message', [id, message]);
    template.find('.private-message').val('')
}
function add_chat_window(socket_id_to) {

    var chat_template = $('#chat-template')
    var chat_container = $('#chat-container')
    var com_class = my_socket_id + '-' + socket_id_to;

    if (chat_container.find('.' + com_class).length == 0) {

        chat_template.find('.template').addClass(com_class)
        chat_container.append(chat_template.html())
        chat_template.find('.template').removeClass(com_class)
    }
    var current_chat = '';
    if (chat_container.find('.' + com_class).length > 0) {
        current_chat = chat_container.find('.' + com_class)
        console.log('finded window ' + com_class)
    }

    current_chat.find('.private-id').val(socket_id_to)
    current_chat.find('.private-name').text(users[socket_id_to])
}