var fs = require('fs');

console.time('run time');

//Cach 1 none blocking
fs.readFile('data1.txt','utf8',function(err, data){
    console.log(data)
})

fs.readFile('data2.txt','utf8',function(err, data){
    console.log(data)
})

//Cach 2  blocking
// var data= fs.readFileSync('data1.txt','utf8');
// var data1= fs.readFileSync('data2.txt','utf8');
// console.log(data)
// console.log(data1)


console.timeEnd('run time');