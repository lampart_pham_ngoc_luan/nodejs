var http = require('http'); //load http.io
var socketIO = require('socket.io'); //load socket.io
var fs = require('fs');

//create a server http:
http.createServer(function (req, res) {

    if(req.url.indexOf('.html') != -1){ //req.url has the pathname, check if it conatins '.html'

        fs.readFile(__dirname +req.url, function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            if (err){
                res.write('Url invalid');
            }
            else{
                res.write(data);
            }
            res.end()
        });

    }

    if(req.url.indexOf('.js') != -1){ //req.url has the pathname, check if it conatins '.js'
        fs.readFile(__dirname + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            res.end();
        });

    }

    if(req.url.indexOf('.css') != -1){ //req.url has the pathname, check if it conatins '.css'

        fs.readFile(__dirname + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.write(data);
            res.end();
        });

    }

}).listen(80); //the server object listens on port 8080

//create server socket
//ip và port connect
var ip = '127.0.0.1';
var port = 3000;
var server = http.createServer().listen(port, ip, function () {
    console.log('Server socket started');
});


//g?n socket.io vào server
io = socketIO.listen(server);


var clients = {};

//connection event
io.sockets.on('connection', function (socket) {

    console.log('connected client id  : ' + socket.id)
    console.log('Object clients : ' + JSON.stringify(clients))

    socket.on('register', function (data) {
        socket.emit('register',socket.id);

        clients[socket.id] = data
        io.sockets.emit('users',clients)
        console.log(socket.id + ' reg name : ' + data)
        console.log('Object clients : ' + JSON.stringify(clients))
    })

    socket.on('message', function (data) {
        if(typeof clients[socket.id] == 'undefined'){
            io.sockets.emit('users',clients)
            return;
        }
        console.log('data : ' + JSON.stringify(data))
        console.log('from:'+socket.id+ ' to:' + data[0]+' message:' + data[1])

        data.push(socket.id)

        if(data[0]=='chatgroup'){
            io.sockets.emit('message',data)
        }
        else{
            socket.to(data[0]).emit('message',data)
            socket.emit('message',data)
        }

    })


    socket.on('disconnect', function (data) {
        console.log('disconnect : '+socket.id)
        delete clients[socket.id]
        io.sockets.emit('users',clients)
    });

});


