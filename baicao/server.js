var http = require('http'); //load http.io
var socketIO = require('socket.io'); //load socket.io
var fs = require('fs');

//create a server http:
http.createServer(function (req, res) {

    if (req.url.indexOf('.html') != -1) { //req.url has the pathname, check if it conatins '.html'

        fs.readFile(__dirname + req.url, function (err, data) {
            res.writeHead(200, {'Content-Type': 'text/html'});
            if (err) {
                res.write('Url invalid');
            }
            else {
                res.write(data);
            }
            res.end()
        });

    }

    if (req.url.indexOf('.js') != -1) { //req.url has the pathname, check if it conatins '.js'
        fs.readFile(__dirname + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/javascript'});
            res.write(data);
            res.end();
        });

    }

    if (req.url.indexOf('.css') != -1) { //req.url has the pathname, check if it conatins '.css'

        fs.readFile(__dirname + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/css'});
            res.write(data);
            res.end();
        });

    }

    if (req.url.indexOf('.png') != -1) { //req.url has the pathname, check if it conatins '.css'

        fs.readFile(__dirname + req.url, function (err, data) {
            if (err) console.log(err);
            res.writeHead(200, {'Content-Type': 'text/html'});
            res.write(data);
            res.end();
        });

    }

}).listen(80); //the server object listens on port 8080

//create server socket
//ip và port connect
var ip = '127.0.0.1';
var port = 3000;
var server = http.createServer().listen(port, ip, function () {
    console.log('Server socket started');
});


//g?n socket.io vào server
io = socketIO.listen(server);


var clients = {};

var cards = []

var session_start = false;

var deposit = 10;

var total_player=0

//connection event
io.sockets.on('connection', function (socket) {

    socket.on('disconnect', function (data) {
        console.log('disconnect : ' + socket.id)
        delete clients[socket.id]
        if(session_start){
            return
        }

        io.sockets.emit('users', clients)
    });

    console.log('connected client id  : ' + socket.id)
    console.log('Object clients : ' + JSON.stringify(clients))

    socket.on('register', function (data) {
        if(session_start){
            return
        }
        socket.emit('register', socket.id);

        clients[socket.id] = {name: data, card: [], winner: false, total_value: 0, money: 1000,plus:0}
        io.sockets.emit('users', clients)
        console.log(socket.id + ' reg name : ' + data)
        console.log('Object clients : ' + JSON.stringify(clients))
    })

    socket.on('message', function (data) {
        if (typeof clients[socket.id] == 'undefined') {
            io.sockets.emit('users', clients)
            return;
        }
        console.log('data : ' + JSON.stringify(data))
        console.log('from:' + socket.id + ' to:' + data[0] + ' message:' + data[1])

        data.push(socket.id)

        if (data[0] == 'chatgroup') {
            io.sockets.emit('message', data)
        }
        else {
            socket.to(data[0]).emit('message', data)
            socket.emit('message', data)
        }

    })


    //chia bai
    socket.on('distribute', function (data) {
        if (session_start) {
            return;
        }
        io.sockets.emit('users', clients)
        cards = shuffleArray(create_cards());
        var resutl = distribute_card(cards)
        console.log(resutl)
        session_start = true
        io.sockets.emit('distribute', resutl)
    })

    socket.on('session_close', function (data) {
        session_start = false
    })


});

function shuffleArray(array) {

    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}
function create_cards() {
    var type = ['bich', 'chuong', 'ro', 'co'];
    var co = [];
    var ro = [];
    var chuong = [];
    var bich = [];
    var value = 0;
    for (var i = 1; i <= 13; i++) {
        value = i;
        var type = 1
        var name = i;
        var is_image = 0;
        if (i == 11) {
            name = 'J'
            is_image = 1
            value = 10
        }
        if (i == 12) {
            name = 'Q'
            is_image = 1
            value = 10
        }
        if (i == 13) {
            name = 'K'
            is_image = 1
            value = 10
        }

        var card = {name: name, value: value, type: type, image: '', is_image: is_image}
        bich.push(card)

        type++
        // value++
        var card = {name: name, value: value, type: type, image: '', is_image: is_image}
        chuong.push(card)

        type++
        // value++
        var card = {name: name, value: value, type: type, image: '', is_image: is_image}
        ro.push(card)

        type++
        // value++
        var card = {name: name, value: value, type: type, image: '', is_image: is_image}
        co.push(card)
    }

    return co.concat(ro, chuong, bich)
}

function distribute_card(cards) {
    var from_slice = 0
    total_player=0

    for (var key in clients) {
        if(clients[key]['money'] > deposit){
            clients[key]['card'] = cards.slice(from_slice, from_slice + 3)
            clients[key]['winner'] = false
            clients[key]['total_value'] = 0
            clients[key]['money'] -= deposit
            clients[key]['plus'] = 0
            from_slice += 3
            total_player++
        }

    }

    return find_winner();
}

function find_winner() {
    var count_winner = 0;
    var winner = []
    var max_value = get_max_value()
    for (var key in clients) {
        if (clients[key].total_value == max_value) {
            count_winner++;
            winner.push(key)
            clients[key]['winner'] = true;
        }
    }

    if(winner.length > 1){
        var plus_win=parseFloat((deposit*total_player)/winner.length).toFixed(2)
        for (var i in winner) {
            clients[winner[i]]['money']=parseFloat(clients[winner[i]]['money'])+parseFloat(plus_win)
            clients[winner[i]]['plus'] = parseFloat(plus_win)
        }
    }
    else{
        clients[winner[0]]['money']+=deposit*total_player
        clients[winner[0]]['plus'] = parseFloat(deposit*total_player)
    }

    return clients;
}

function get_max_value() {
    var max_value = 0;
    for (var key in clients) {
        if (clients[key]['card']) {
            var cards = clients[key]['card']
            var total_value = 0;
            var three_image = 0;
            for (var card in cards) {
                var card_value = parseInt(cards[card].value)
                if (cards[card].is_image == 1) {
                    three_image++;
                }
                if (cards[card].value == 10) {
                    card_value = 0
                }
                total_value += card_value
                if (total_value >= 10) {
                    total_value -= 10
                }
                if (three_image == 3) {
                    total_value = 30
                }
            }

        }
        clients[key]['total_value'] = total_value
        if (total_value > max_value) {
            max_value = total_value
        }
    }

    return max_value;
}

